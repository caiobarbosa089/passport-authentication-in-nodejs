const jwt = require('express-jwt');

/** Extract token from request headers */
const getTokenFromHeaders = (req) => {
	console.log('extracting headers: ', req.headers.authorization);
	if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
};

// Object with different middleware configurations to access endpoints
const auth = {
	required: jwt({
		secret: 'my-secret',
		userProperty: 'payload',
		getToken: getTokenFromHeaders,
	}),
	optional: jwt({
		secret: 'my-secret',
		userProperty: 'payload',
		getToken: getTokenFromHeaders,
		credentialsRequired: false,
	}),
};

module.exports = auth;
