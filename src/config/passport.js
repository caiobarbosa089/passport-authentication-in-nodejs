const passport = require('passport');
const LocalStrategy = require('passport-local');
const users = require('../data/users.json'); // Collection of valid users for tests

passport.use(new LocalStrategy({
  usernameField: 'user[username]',
  passwordField: 'user[password]',
}, (username, password, done) => {
	console.log('-- entered middleware --');
	console.log('user:', username, 'password:', password);
	try {
		const user = users.filter(x => x.username === username && x.password === password)[0];
		if (user) {
			if (user.active)
				return done(null, user); // OK
			else
				return done(null, false, { error: 'User is inactive, contact admin.' })
		} else
			return done(null, false, { error: 'Invalid data provided.' });
	} catch (e) {
		done(e);
	}
}));
