const express = require('express');
// const session = require('express-session');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const errorHandler = require('errorhandler');

require('./config/passport');

const app = express();
app.use(cors());
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(errorHandler());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(session({ secret: 'passport-tutorial', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
app.use(passport.initialize());

app.get('/', (req, res) => res.send('Welcome, you are in the root of the API server.'))
app.use('/users', require('./routes/users'));

// Error handler middleware
app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') res.status(401).json({ message: 'invalid token...' });
	res.status(500).json({ message: 'Something wrong happens.', error: err });
});

app.listen(4000, () => console.log('Server running successfully at 4000.'));
