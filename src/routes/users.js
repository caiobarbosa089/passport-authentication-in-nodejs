const passport = require('passport');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const auth = require('../auth');
const users = require('../data/users.json'); // Collection of valid users for tests

const generateJWT = (user) => {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  return jwt.sign({
		_id: user._id,
    username: user.username,
    name: user.name,
    email: user.email,
    exp: parseInt(expirationDate.getTime() / 1000, 10),
  }, 'my-secret');
}

router.get('/', (req, res) => res.send('Entered users API'));

/** POST login route (optional, everyone has access) */
router.post('/login', auth.optional, (req, res, next) => {
  const { body: { user } } = req;

  if(!user.username) return res.status(422).json({ errors: { username: 'is required' }});
  if(!user.password) res.status(422).json({ errors: { password: 'is required' }});

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {

    if (err) return next(err);

    if(passportUser) {
      return res.json({
				authenticated: true,
				user: {
					_id: user._id,
					username: user.username,
					token: generateJWT(passportUser)
				}
			});
    }

    return status(400).info;
  })(req, res, next);
});

/** GET current route (required, only authenticated users have access) */
router.get('/current', auth.required, (req, res, next) => {
  const { payload: { _id } } = req;

	const user = users.filter(user => user._id === parseInt(_id))[0] || null;

	if (user)	res.json({ user });

	res.status(522).json({ message: 'User not found!' });
  // return Users.findById(id)
  //   .then((user) => {
  //     if(!user) {
  //       return res.sendStatus(400);
  //     }
	//
  //     return res.json({ user: user.toAuthJSON() });
  //   });
});

module.exports = router;
